﻿using fr34kyn01535.Uconomy;
using Rocket.API.Collections;
using Rocket.API;
using Rocket.Core.Logging;
using Rocket.Core.Plugins;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using System;
using System.IO;
using System.Net;

namespace Charter.HomeFast
{
    public class HomeFast : RocketPlugin<HomeFastConfiguration>
    {
        protected override void Load()
        {
            Console.WriteLine("Charter.HomeFast loading...");
            Console.WriteLine("Now cost for /homefast is " + Configuration.Instance.CostHomeFast);
            Console.WriteLine("Charter.HomeFast loaded!");
        }

        protected override void Unload()
        {
            Logger.Log("Charter.HomeFast was Unloaded!");
        }

        public override TranslationList DefaultTranslations
        {
            get
            {
                return new TranslationList()
                {
                    {"error_invalid_args","Error code: 1. Invalid args."},
                    {"home_not_found","Your home not found! You dont have claimed bed!"},
                    {"home_success","You was succesfully teleported to your claimed bed."},
                    {"no_money","You dont have money for HomeFast!"}
                };
            }
        }

        public static HomeFast Instance;

        public void GoHome(UnturnedPlayer player)
        {
            if (!BarricadeManager.tryGetBed(player.CSteamID, out var bedPosition, out var bedAngle))
            {
                UnturnedChat.Say(player, Translate("home_not_found"));
                Console.WriteLine("Bed of " + player + " not found.");
                return;
            }
            else
            {
                decimal myBalance = Uconomy.Instance.Database.GetBalance(player.Id);
                if ((myBalance - HomeFast.Instance.Configuration.Instance.CostHomeFast) <= 0)
                {
                    UnturnedChat.Say(player, Translate("no_money"));
                    return;
                } 
                else
                {
                    player.Teleport(bedPosition, player.Rotation);
                    UnturnedChat.Say(player, Translate("home_success"));
                    Console.WriteLine("Succesfully teleport player " + player + " to his bed.");
                    Uconomy.Instance.Database.IncreaseBalance(player.Id, -HomeFast.Instance.Configuration.Instance.CostHomeFast);
                }
            }
        }
    }
}
