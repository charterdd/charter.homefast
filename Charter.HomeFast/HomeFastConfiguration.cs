﻿using Rocket.API;

namespace Charter.HomeFast
{
    public class HomeFastConfiguration : IRocketPluginConfiguration
    {
        public int CostHomeFast;
        public void LoadDefaults()
        {
            CostHomeFast = 1000;
        }
    }
}
