﻿using Rocket.API;
using Rocket.Unturned.Player;
using System.Collections.Generic;

namespace Charter.HomeFast
{
    class CommandHomeFast : IRocketCommand
    {
        public string Name
        {
            get
            {
                return "homefast";
            }
        }
        public string Help
        {
            get
            {
                return "/homefast";
            }
        }
        public string Syntax
        {
            get
            {
                return "/homefast";
            }
        }
        public List<string> Aliases
        {
            get
            {
                return new List<string>
                {

                };
            }
        }
        public List<string> Permissions
        {
            get
            {
                return new List<string>
                {
                    "charter.command.homefast"
                };
            }
        }
        public AllowedCaller AllowedCaller
        {
            get
            {
                return AllowedCaller.Player;
            }
        }

        public void Execute(IRocketPlayer caller, string[] msg)
        {
            UnturnedPlayer player = (UnturnedPlayer)caller;
            HomeFast.Instance.GoHome(player);
        }
    }
}
